module Jekyll
    
    class RenderImagePosition < Liquid::Tag
    
        def initialize(tag_name, text, tokens)
            super
            @text = text
        end

        def render(context)
	    "<!-- .slide: data-background-position=\"#{@text}\" -->"
            #"<!-- .slide: data-background=\"@text\" -->"
        end

    end

end

Liquid::Template.register_tag('imgpos', Jekyll::RenderImagePosition)
