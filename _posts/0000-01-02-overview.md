## Características do OpenShift

Versão **Origin v3** Community Version

Proposta de **PaaS** - Platform as a Service

Agrega recursos ao Kubernetes (K8s) 

- ImageStream
- BuildConfig
- DeploymentConfig
- Routes, etc

---

## Triggers DeploymentConfig

Permite criar uma nova "versão" do DC através de gatilhos como:

- Mudança de Configuração do DC
- Mudança de uma ImageStream

```
  triggers:
  - type: ConfigChange
  - imageChangeParams:
      automatic: true
      containerNames:
      - web
      from:
        kind: ImageStreamTag
        name: mastodon:latest
    type: ImageChange
```

---

Visão geral do **Cluster H.A** OpenShift

![cluster](https://gitlab.com/thiagolinux/meetupdockerdf/raw/pages/images/cluster-ha.png)
