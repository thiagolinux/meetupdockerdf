module Jekyll
    
    class RenderImage < Liquid::Tag
    
        def initialize(tag_name, text, tokens)
            super
            @text = text
        end

        def render(context)
	    "<!-- .slide: data-background-image=\"custom_themes/images/#{@text}\" -->"
            #"<!-- .slide: data-background=\"@text\" -->"
        end

    end

end

Liquid::Template.register_tag('img', Jekyll::RenderImage)
